from common import byte_converter, number_of_files, process_input, random_chars
import os
import random
import sys


def generate_data(directory, file_name, size, file_size):
    print("Data generation in progress")
    content = []
    num_files = number_of_files(size, file_size)
    last_size = byte_converter(size - ((num_files - 1) * file_size))
    current_path = os.path.abspath(os.curdir)
    path = os.path.join(current_path, directory, file_name)
    if not os.path.exists(path):
        os.makedirs(path)
    # Generate files just before the last one
    for i in range(0, num_files - 1):
        file_size_processing = byte_converter(file_size)
        while file_size_processing > 0:
            print('.', end='')
            random_number = random.randint(8, 32)
            if file_size_processing > random_number:
                content.append(next(random_chars(random_number)) + "\n")
                file_size_processing -= random_number
            else:
                content.append(next(random_chars(file_size_processing)))
                file_size_processing = 0
        print()
        file = os.path.join(path, "file" + str(i + 1))
        with open(file, "w") as f:
            f.writelines(content)
        content = []
    # Generate the last file
    content = []
    while last_size > 0:
        random_number = random.randint(60, 64)
        if last_size > random_number:
            content.append(next(random_chars(random_number)) + "\n")
            last_size -= random_number
        else:
            content.append(next(random_chars(size)))
            last_size = 0
    file = os.path.join(path, "file" + str(num_files))
    with open(file, "w") as f:
        f.writelines(content)


if __name__ == '__main__':
    if 3 > len(sys.argv):
        print("Three commandline arguments are expected.")
        print("format: master_dataset_location fileSize file1,size1,file2,size2...")
        print("e.g. masterDataset locations,12,sensors,23,devices,10")
        exit(0)
    masterDataSet = sys.argv[1]
    fileSize = int(sys.argv[2].strip())
    locationSizeDict = process_input(sys.argv[3])
    for k, v in locationSizeDict.items():
        generate_data(masterDataSet, k, v, fileSize)
    print("Done.")
