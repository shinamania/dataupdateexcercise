from common import byte_converter, process_input, random_chars
import os
import random
import sys


def list_files(directory, file_name):
    path = os.path.join(os.path.abspath(os.curdir), directory, file_name)
    return [os.path.join(path, file) for file in os.listdir(path)]


def update_data(directory, file_name, file_size):
    files = list_files(directory, file_name)
    num_files = len(files)
    file_size = byte_converter(file_size)
    last_file_size = file_size % (num_files - 1)
    file_size_processing = (file_size - last_file_size) // num_files - 1
    last_file = files.pop()
    content = []
    for file in files:
        print("Data update in progress")
        while file_size_processing > 0:
            print("File size processing: %s" % file_size_processing)
            print('.', end='')
            random_number = random.randint(60, 64)
            if file_size_processing > random_number:
                content.append("\n" + next(random_chars(random_number)) + "\n")
                file_size_processing -= random_number
            else:
                content.append(next(random_chars(file_size_processing)))
                file_size_processing = 0
        print()
        print("File: {}".format(file))
        with open(file, "a") as f:
            f.writelines(content)
    # Update the last file
    content = []
    while last_file_size > 0:
        random_number = random.randint(60, 64)
        if last_file_size > random_number:
            content.append(next(random_chars(random_number)) + "\n")
            last_file_size -= random_number
        else:
            content.append(next(random_chars(last_file_size)))
            last_file_size = 0
    print("File: {}, last file size: {}".format(last_file, last_file_size))
    with open(last_file, "a") as f:
        f.writelines(content)


if __name__ == '__main__':
    if 2 > len(sys.argv):
        print("Two commandline arguments are expected.")
        print("format: master_dataset_location file1,size1,file2,size2...")
        print("e.g. masterDataset locations,12,sensors,23,devices,10")
        exit(0)
    masterDataSet = sys.argv[1]
    locationSizeDict = process_input(sys.argv[2])
    for k, v in locationSizeDict.items():
        update_data(masterDataSet, k, v)
    print("Done.")
