from itertools import islice
from string import ascii_uppercase
import random


def byte_converter(megabyte):
    return megabyte * 1000000


def number_of_files(size, file_size):
    return int(size / file_size)


def process_input(file_sizes):
    file_sizes = file_sizes.split(',')
    output = []
    for i in range(0, len(file_sizes), 2):
        output.append((file_sizes[i], int(file_sizes[i + 1])))
    output_dict = dict(output)
    return output_dict


def random_chars(size, chars=ascii_uppercase):
    """

    :param size: length of a line
    :type size: int
    :param chars: encoding
    :type chars: basestring
    """
    selection = iter(lambda: random.choice(chars), object())
    while True:
        yield ''.join(islice(selection, size))
